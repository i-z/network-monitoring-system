#Network Monitoring System

Network Monitoring System is team effort for the [Distributed Embeded Systems](http://dsnet.tu-plovdiv.bg/website/) discipline in the [Technical University of Sofia, Plovdiv branch](http://www.tu-plovdiv.bg/en/).

## Team Members
* Anton Tonev - toni9290@abv.bg
* Velizar Minchev - velizar.minchev@yahoo.com
* Ivan Zdravkov - ivanzdravkovbg@gmail.com
* Trifon Dardjonov - trifon.dardzhonov@gmail.com

## Setup
* [SourceTree](https://www.sourcetreeapp.com/) - git client. (optional, other git clients are valid as well)
* [Python 3.6.1](https://www.python.org/downloads/) - Python.
* [MongoDB](https://www.mongodb.com/) - NoSQL Database